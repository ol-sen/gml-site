#!/usr/bin/env python3

import os
import argparse
import time
import selenium
import selenium.webdriver
from selenium.webdriver.common.by import By
from parse_publications import parse_people_info


def update_publicaions(args):
    with open(args.istina_username_password_path, "r") as fp:
        username, password = fp.readlines()[:2]
    info_for_people_parsing = parse_people_info()

    options = selenium.webdriver.chrome.options.Options()
    options.add_argument("--headless")
    driver = selenium.webdriver.Chrome('./chromedriver', options=options)
    driver.get('https://istina.msu.ru/accounts/login/')
    driver.find_element(By.NAME, "username").send_keys(username)
    driver.find_element(By.NAME, "password").send_keys(password)
    # driver.find_element_by_xpath("//input[@type='submit' and @value='Войти']") #.click()
    for person in info_for_people_parsing:
        if person["istina_path"] == "":
            continue
        print("loading page:", person["istina_path"])
        driver.get(person["istina_path"])
        bib_href = driver.find_element_by_link_text('Экспорт публикаций в BibTeX').get_attribute("href")
        driver.get(bib_href)
        while driver.current_url == person["istina_path"]:
            time.sleep(1)
        bibs = driver.find_elements_by_tag_name('body')[0].text
        with open("temp_bib.bib", "w") as fp:
            fp.write(bibs)
        print("processing page:", person["istina_path"])
        for group_id in person["group_ids"]:
            os.system("python3 parse_publications.py "
                      "--update_bib_path temp_bib.bib "
                      "--update_group_id " + group_id)
    driver.quit()


def parse_args():
    parser = argparse.ArgumentParser(usage='python3 parse_publications.py <args>')
    parser.add_argument('--istina_username_password_path', help='Путь до файла с логином и паролем от Истины',
                        type=str, required=False, default="istina_username_password.txt")
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()
    update_publicaions(args)
