# -*- coding: utf-8 -*-

import os
import os.path
import copy
import csv
import json
import bibtexparser
import functools
from operator import attrgetter

from pelican import signals
from pelican.generators import ArticlesGenerator


def GetPageUrl(settings, slug):
    url = '{slug}.html'.format(slug=slug)
    return url


def SplitMdList(list_str):
    if list_str.strip() == '':
        return []
    else:
        return list(list_str.strip().split(', '))


class Holder(object):
    # First element always should be 'id'
    INFORMATION_FIELDS = ['id']
    category_name = ''

    def __init__(self, categories, settings, id_field='id'):
        self.settings = settings
        articles = sum([v for k, v in categories if k == self.category_name], [])
        self.objects = []
        self.id_to_objects_pos = {}
        for article in articles:
            if hasattr(article, 'active') and article.active != 'yes':
                continue
            article.id = getattr(article, id_field)
            self.id_to_objects_pos[article.id] = len(self.objects)
            self.objects.append(article)
        self.missing_ids_info = {}

    def get_info_by_ids(self, need_ids):
        return_infos = []
        for cur_id in need_ids:
            cur_object = None
            if cur_id in self.id_to_objects_pos.keys():
                cur_object = self.objects[self.id_to_objects_pos[cur_id]]
            if cur_object is None:
                cur_return_info = {
                    field_name: 'None' for field_name in self.INFORMATION_FIELDS
                }
                if cur_id in self.missing_ids_info:
                    for field_name in self.INFORMATION_FIELDS:
                        if field_name in self.missing_ids_info[cur_id]:
                            cur_return_info[field_name] = self.missing_ids_info[cur_id][field_name]

                cur_return_info['id'] = cur_id
                return_infos.append(cur_return_info)
            else:
                return_infos.append({
                    field_name: getattr(cur_object, field_name) for field_name in self.INFORMATION_FIELDS
                })
        return return_infos


class PeopleHolder(Holder):
    INFORMATION_FIELDS = ['id', 'name_surname', 'url']
    category_name = 'people'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [persontype, id, title, name_surname, name, surname,
         email, photo, position, researchinterests,
         projects, selectedpublications, about, url]
        '''
        super().__init__(categories, settings)
        for person in self.objects:
            person.about = person.content
            person.name = person.name.strip()
            person.surname = person.surname.strip()
            person.name_surname = ' '.join([person.name, person.surname])
            person.researchinterests = SplitMdList(person.researchinterests)
            person.projects = SplitMdList(person.projects)
            if hasattr(person, 'selectedpublications'):
                person.selectedpublications = SplitMdList(person.selectedpublications)

        with open(settings['PATH_TO_MISSING_ID_TO_PEOPLE_INFO'], "r") as fp:
            reader = csv.DictReader(fp)
            for row in reader:
                self.missing_ids_info[row['id']] = row


class ProjectsHolder(Holder):
    INFORMATION_FIELDS = ['id', 'smallphoto', 'title', 'url']
    category_name = 'projects'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [projecttype, id, smallphoto, photo,
         title, contactperson, team, selectedpublications, content]
        '''
        super().__init__(categories, settings)
        for project in self.objects:
            project.selectedpublications = SplitMdList(project.selectedpublications)
            project.team = SplitMdList(project.team)

        with open(settings['PATH_TO_MISSING_ID_TO_PROJECTS_INFO'], "r") as fp:
            reader = csv.DictReader(fp)
            for row in reader:
                self.missing_ids_info[row['id']] = row


class MyPublicationEntry:
    def __init__(self, bib_entry=None, json_info=None):
        if bib_entry is not None:
            db = bibtexparser.bibdatabase.BibDatabase()
            db.entries = [copy.copy(bib_entry)]
            self.id = bib_entry['ID']
            if 'annote' in bib_entry:
                del db.entries[0]['annote']
            self.bibtex = bibtexparser.dumps(db)
            bibtex_lines = list(self.bibtex.strip().split('\n'))
            self.bibtex = "<pre><code>" + \
                          "\n   ".join(bibtex_lines[:-1]) + \
                          "\n" + bibtex_lines[-1] + "</code></pre>"
        elif json_info is not None:
            self.id = json_info['id']
            self.title = json_info['title']
            self.date = json_info['date']
            self.year = json_info['year']
            self.paperauthors = json_info['paperauthors']
            self.labgroupids = json_info['lab_group_ids']
            self.journal = json_info['journal']
            self.abstract = json_info['abstract'].strip()
            self.journallink = json_info['journal_link']
            self.bibtex = ""
            self.partofprojects = []
            if 'pdf_path' in json_info:
                self.pdfpath = json_info['pdf_path']
            if 'bib_tex_id' in json_info:
                self.bibtexid = json_info['bib_tex_id']


class PublicationsHolder(Holder):
    INFORMATION_FIELDS = ['id', 'title', 'journal', 'journallink', 'paperauthors', 'year']
    category_name = 'publications'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [id, photo, title, paperauthors, journal, downloadlink, content]
        '''
        self.settings = settings
        self.objects = []
        self.id_to_objects_pos = {}

        with open(settings["PUBLICATIONS_INFO_PATH"], 'r') as fp:
            publications_data = json.load(fp)
        for publ_info in publications_data:
            cur_publication = MyPublicationEntry(json_info=publ_info)
            cur_publication.pdfpath = cur_publication.pdfpath.format(static=settings["SITEURL"])
            self.id_to_objects_pos[cur_publication.id] = len(self.objects)
            self.objects.append(cur_publication)

        bib_publication_entries = []
        bib_publication_entries_ids = {}

        with open(settings['PUBLICATIONS_BIB_PATH'], 'r') as fp:
            bib_database = bibtexparser.bparser.BibTexParser().parse_file(fp)
            for bib_entry in bib_database.entries:
                cur_publication = MyPublicationEntry(bib_entry=bib_entry)
                if cur_publication.id not in bib_publication_entries_ids:
                    bib_publication_entries.append(cur_publication)
                    bib_publication_entries_ids[cur_publication.id] = len(bib_publication_entries) - 1

        for publication in self.objects:
            if publication.bibtexid in bib_publication_entries_ids:
                publication.bibtex = bib_publication_entries[bib_publication_entries_ids[publication.bibtexid]].bibtex
                self.id_to_objects_pos[publication.bibtexid] = self.id_to_objects_pos[publication.id]
            else:
                publication.bibtex = publication.bibtexid

        with open(settings['SELCETED_PUBLICATIONS_PATH'], 'r') as fp:
            self.selected_publications_ids = json.load(fp)

    @staticmethod
    def papers_comparator(x, y):
        if hasattr(x, 'date') and hasattr(y, 'date'):
            if x.date < y.date:
                return 1
            elif x.date > y.date:
                return -1
            else:
                return 0
        else:
            if x.year < y.year:
                return 1
            elif x.year > y.year:
                return -1
            else:
                return 0


class NewsHolder(Holder):
    INFORMATION_FIELDS = []
    category_name = 'news'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [title, date, content]
        '''
        super().__init__(categories, settings, 'slug')


class CoursesHolder(Holder):
    INFORMATION_FIELDS = ['id', 'title', 'teachers']
    category_name = 'courses'

    def __init__(self, categories, settings):
        '''
        Доступные поля в objects:
        [title, id, teachers, content]
        '''
        super().__init__(categories, settings)
        for cur_course in self.objects:
            cur_course.teachers = SplitMdList(cur_course.teachers)


class DataHolder(object):
    def __init__(self, categories, settings):
        self.projects_holder = ProjectsHolder(categories, settings)
        self.people_holder = PeopleHolder(categories, settings)
        self.publications_holder = PublicationsHolder(categories, settings)
        self.news_holder = NewsHolder(categories, settings)
        self.courses_holder = CoursesHolder(categories, settings)

        for publication_article in self.publications_holder.objects:
            paperauthors_info = self.people_holder.get_info_by_ids(
                [author['person_id'] for author in publication_article.paperauthors])
            for author, info in zip(publication_article.paperauthors, paperauthors_info):
                author['person_id'] = info

        for course_article in self.courses_holder.objects:
            course_article.teachers = self.people_holder.get_info_by_ids(course_article.teachers)

        for person_article in self.people_holder.objects:
            person_article.projects = self.projects_holder.get_info_by_ids(person_article.projects)
            if hasattr(person_article, 'selectedpublications'):
                person_article.selectedpublications = \
                    self.publications_holder.get_info_by_ids(person_article.selectedpublications)
            else:
                cur_person_publications = []
                for publication in self.publications_holder.objects:
                    if person_article.id in [elem['person_id']['id'] for elem in publication.paperauthors]:
                        cur_person_publications.append(publication)
                cur_person_publications.sort(key=functools.cmp_to_key(PublicationsHolder.papers_comparator))
                cur_person_publications = [publication.id for publication in cur_person_publications]
                cur_person_publications = cur_person_publications[:5]
                person_article.recentpublications = self.publications_holder.get_info_by_ids(cur_person_publications)

        for project_article in self.projects_holder.objects:
            project_article.contactperson = self.people_holder.get_info_by_ids([project_article.contactperson])
            if len(project_article.contactperson) > 0:
                project_article.contactperson = project_article.contactperson[0]
            project_article.team = self.people_holder.get_info_by_ids(project_article.team)
            for publication in project_article.selectedpublications:
                if publication in self.publications_holder.id_to_objects_pos:
                    self.publications_holder.objects[self.publications_holder.id_to_objects_pos[publication]].partofprojects.append(project_article.id)
            project_article.selectedpublications = self.publications_holder.get_info_by_ids(project_article.selectedpublications)

        for publication_article in self.publications_holder.objects:
            publication_article.partofprojects = self.projects_holder.get_info_by_ids(publication_article.partofprojects)


class MyGenerator(ArticlesGenerator):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_output(self, writer):
        # print([(elem.url, [tr.url for tr in elem.translations]) for elem in self.articles])
        data = DataHolder(self.categories, self.settings)
        write = functools.partial(writer.write_file, relative_urls=self.settings['RELATIVE_URLS'], context=self.context)

        self.all_article_identifiers = {
            (str(article.category), str(article.id)): str(article.source_path) for article in self.articles
        }
        self.used_article_identifiers = set()

        self.generate_homepage(write, data)
        self.generate_news(write, data)
        self.generate_people_page(write, data)
        self.generate_persons(write, data)
        self.generate_projects_page(write, data)
        self.generate_projects(write, data)
        self.generate_publications_page(write, data)
        self.generate_courses_page(write, data)
        signals.article_writer_finalized.send(self, writer=writer)
        self.check_identifiers()

    def use_identifiers(self, objects):
        for obj in objects:
            self.used_article_identifiers.add((str(obj.category), str(obj.id)))

    def check_identifiers(self):
        for k, v in self.all_article_identifiers.items():
            if k not in self.used_article_identifiers:
                print("\033[91m", "FOUND UNUSED PAGE:", v, "\033[0m")

    def generate_homepage(self, write, data):
        url = save_as = GetPageUrl(self.settings, 'home')
        all_news_link = GetPageUrl(self.settings, 'newslist')

        current_news = [news for news in data.news_holder.objects]
        current_news.sort(key=attrgetter('date'), reverse=True)
        current_news = current_news[:7]
        self.use_identifiers(current_news)

        write(save_as, self.get_template('home'), template_name='home',
              articles=current_news, all_news_link=all_news_link,
              page_name=os.path.splitext(save_as)[0], url=url)

    def generate_news(self, write, data):
        url = save_as = GetPageUrl(self.settings, 'newslist')

        current_news = [news for news in data.news_holder.objects]
        current_news.sort(key=attrgetter('date'), reverse=True)
        self.use_identifiers(current_news)

        write(save_as, self.get_template('news'), template_name='news',
              articles=current_news, output_menu_type='/home.html',
              page_name=os.path.splitext(save_as)[0], url=url)

    def generate_people_page(self, write, data):
        PEOPLE_PAGES = [
            {
                "page_id": "people",
                "page_title": "Staff",
                "people_types": ['staff', 'phd', 'masters', 'bachelor'],
                "people_types_titles": ['Staff', 'PhD students', 'Masters students', 'Bachelor students'],
            },
            {
                "page_id": "alumni",
                "page_title": "Alumni",
                "people_types": ['alumni'],
                "people_types_titles": ['Alumni'],
            },
        ]
        for page in PEOPLE_PAGES:
            page["url"] = GetPageUrl(self.settings, page["page_id"])
        for page in PEOPLE_PAGES:
            save_as = url = page["url"]
            people_data = []
            for people_type, type_header in zip(page["people_types"], page["people_types_titles"]):
                current_people = [person for person in data.people_holder.objects if person.persontype == people_type]
                current_people.sort(key=lambda x: float(x.personorder))
                people_data.append([type_header, people_type, current_people])
                self.use_identifiers(current_people)

            write(save_as, self.get_template('people_page'), template_name='people_page',
                  people_data=people_data, all_pages_info=PEOPLE_PAGES,
                  page_name=os.path.splitext(save_as)[0], url=url)

    def generate_persons(self, write, data):
        self.use_identifiers(data.people_holder.objects)
        for person_article in data.people_holder.objects:
            url = save_as = person_article.url
            write(save_as, self.get_template('person_page'), template_name='person_page',
                  person_info=person_article, output_menu_type='/people.html',
                  page_name=os.path.splitext(save_as)[0], url=url)

    def generate_projects_page(self, write, data):
        PROJECT_TYPES = ['ongoing', 'completed']
        PROJECT_TYPES_NAMES = ['projects_ongoing', 'projects_completed']

        projects_output = {}
        for project_type, project_type_name in zip(PROJECT_TYPES, PROJECT_TYPES_NAMES):
            url = save_as = GetPageUrl(self.settings, 'projects')

            current_projects = [project for project in data.projects_holder.objects if project.projecttype == project_type]
            current_projects.sort(key=lambda x: float(x.projectorder), reverse=True)
            projects_output[project_type_name] = current_projects
            self.use_identifiers(current_projects)

        write(save_as, self.get_template('projects_page'), template_name='projects_page',
              **projects_output,
              page_name=os.path.splitext(save_as)[0], url=url)

    def generate_projects(self, write, data):
        self.use_identifiers(data.projects_holder.objects)
        for project_article in data.projects_holder.objects:
            url = save_as = project_article.url
            write(save_as, self.get_template('project_page'), template_name='project_page',
                  project_info=project_article, output_menu_type='/projects.html',
                  page_name=os.path.splitext(save_as)[0], url=url)

    def generate_publications_page(self, write, data):
        url = save_as = GetPageUrl(self.settings, 'publications')

        all_publications_list = [publication for publication in data.publications_holder.objects]
        possible_years = sorted(list(set([publication.year for publication in all_publications_list])), reverse=True)
        possible_groups = [[elem['group_id'], elem['group_title']] for elem in self.settings['LAB_GROUPS_META_INFO']]
        publications_by_years = []
        for year in possible_years:
            cur_publications_list = [publication for publication in all_publications_list if publication.year == year]
            # list.sort must be stable!
            cur_publications_list.sort(key=functools.cmp_to_key(PublicationsHolder.papers_comparator))
            cur_publications_list = [[publication.id, publication] for publication in cur_publications_list]
            publications_by_years.append((year, cur_publications_list))

        write(save_as, self.get_template('publications_page'), template_name='publications_page',
              publications_by_years=publications_by_years, possible_groups=possible_groups,
              selected_publications_ids=data.publications_holder.selected_publications_ids,
              page_name=os.path.splitext(save_as)[0], url=url)

    def generate_courses_page(self, write, data):
        url = save_as = GetPageUrl(self.settings, 'courses')

        current_courses = [course for course in data.courses_holder.objects]
        current_courses.sort(key=lambda x: float(x.courseorder), reverse=False)
        self.use_identifiers(current_courses)

        write(save_as, self.get_template('courses_page'), template_name='courses_page',
              current_courses=current_courses,
              page_name=os.path.splitext(save_as)[0], url=url)


def create_generator(pelican_object):
    return MyGenerator


def fix_default_articles(article_generator):
    if isinstance(article_generator, MyGenerator):
        return
    # Чтобы добавить новые поля (и изменить существующие) к новостям нужно сконструировать DataHolder
    DataHolder(article_generator.categories, article_generator.settings)
    # Чтобы оставить только новости (так как дефолтным генератором делаются только они)
    article_generator.articles = [article for article in article_generator.articles if article.category == 'news']
    article_generator.translations = [article for article in article_generator.translations if article.category == 'news']


def register():
    signals.get_generators.connect(create_generator)
    signals.article_generator_finalized.connect(fix_default_articles)
