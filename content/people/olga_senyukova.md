Active: yes
Category: people
PersonType: staff
Id: olga_senyukova
Name: Olga
Surname: Senyukova
PublicationsPossibleNames: Ольга
PublicationsPossibleSurnames: Сенюкова
ParseNewPublications: yes
LabGroups: biomedical_group
PersonOrder: 15
Position: Teaching assistant
Email: olga.senyukova@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/Olsen/
Photo: images/people/olga_senyukova.jpg
ResearchInterests: computer vision, machine learning, biomedical image analysis
Projects: medical_image_analysis
SelectedPublications: gavrishchaka15multicomplexity, senyukova14segmentation, gavrishchaka13robust, senyukova11ensemble

I am an assistant professor of the department of Computing Systems and
Automation, Faculty of Computational Mathematics and Cybernetics, Lomonosov
Moscow State University. Currently my group has a joint work with
[Diagnostics and Telemedicine Center of the Moscow Health Care Department](https://tele-med.ai/) and also with
[research group from the USA](http://www.medpobs.com/). My scientific interests include but are not
limited to computer vision and machine learning in disease diagnostics,
prediction of MRI hardware failures, development of personalized medicine
therapies.

**PhD Thesis**: Algorithms for semantic segmentation and classification of low-dimensional biomedical signals based on machine learning
