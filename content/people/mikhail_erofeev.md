Active: yes
Category: people
PersonType: staff
Id: mikhail_erofeev
Name: Mikhail
Surname: Erofeev
PublicationsPossibleNames: Михаил
PublicationsPossibleSurnames: Ерофеев
ParseNewPublications: yes
LabGroups: video_group
PersonOrder: 40
Position: Junior researcher
Email: merofeev@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/merofeev/
GoogleScholarPage: https://scholar.google.com/citations?user=ue-TQbIAAAAJ
Photo: images/people/mikhail_erofeev.jpg
ResearchInterests: video and image processing, machine learning
Projects:
SelectedPublications: erofeev15perceptually, bokov14automatic, gitman14semiautomatic

About Mikhail Erofeev

**PhD Thesis**: Development of methods of high-quality stereo generation for scences with semitransparent edges

## Scholarships and grants

- The Russian President scholarship for young scientists and PhD students. (2015-2017)
- MSU scholarship for young teachers and researchers (2015)
- Grant U.M.N.I.K, "Stereo image processing for 3D enabled cell phones" (2014-2015)
