Active: yes
Category: people
PersonType: staff
Id: dmitriy_kulikov
Name: Dmitriy
Surname: Kulikov
PublicationsPossibleNames: Дмитрий
PublicationsPossibleSurnames: Куликов
ParseNewPublications: yes
LabGroups: video_group
PersonOrder: 40
Position: Codec analysis team leader
Email: dkulikov@graphics.cs.msu.ru
Photo: images/people/dmitriy_kulikov.jpg
ResearchInterests: videocodec analysis, image and video quality metrics, video processing, machine learning, computer vision
Projects:

Dmitriy Kulikov graduated from the Applied Mathematics department of Moscow
State University in 2005. He defended his Ph.D. thesis on computer graphics in
2009. I work in Graphics and Media Lab and have participated in several
projects of the lab, including a project with Samsung, Intel, Real networks.

Most of my research focuses on video processing problems: logo and subtitle
removal, denoising, brightness&contracts enhancement, color enhancement, etc.

Other topic of my research work was devoted video codec analysis. I am project
head for most of MSU Video codec analysis projects since 2005.

**PhD Thesis**: Методы маскирования искажений в видео потоке после сбоев в работе кодека

