Active: yes
Title: Grant of the President of the Russian Federation
Category: news
Date: 2012-02-02 00:00

Anton Konushin has been awarded a grant of the President of the Russian Federation for the young scientists. The topic of the project is "Content-based search in video archives". Vladimir Kononov will be a collaborator in this grant.
