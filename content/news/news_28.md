Active: yes
Title: Defense of Ph.D. thesis of Olga Barinova
Category: news
Date: 2010-11-30 00:00

Defense of Ph.D. thesis of Olga Barinova will take place on December 17th, 2010, at 11 a.m. in room 685 at CMC department of MSU. (you can [download thesis](http://cs.msu.ru/science/1.4/1.4.2.2/201011-bov.doc) in russian)
