Active: yes
Title: Поздравляем лауреатов конкурса выпускных работ!
Category: news
Date: 2015-06-25 00:00

Пять выпускников лаборатории были отмечены на конкурсе выпускных работ студентов ВМК МГУ. Поздравляем Багрова Никиту с получением диплома 2-й степени за 2-е место и Бокова Александра за лучшую практическую работу! Также грамотами за участие в конкурсе были награждены Шахуро Владислав, Старцев Михаил и Зобнин Денис.
