Active: yes
Title: Поздравляем Алексея Федорова с попаданием в число победителей Программы "УМНИК" 2014 и получением гранта
Category: news
Date: 2014-11-19 00:00

Поздравляем Алексея Федорова с попаданием в число победителей [осеннего финала Программы "УМНИК" 2014](http://umnik-msu.ru/) и получением гранта на проект "Разработка автоматического метода определения порядка ракурсов 3D-видео".
