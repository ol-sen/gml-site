Active: yes
Title: Maxim Fedyukov successfully defended his PhD thesis.
Category: news
Date: 2016-03-29 00:00

We cogratulate our former PhD student, Maxim Feduykov, who has successfully defended his PhD thesis today, March 29, 2016! The topic was "Methods of 3D human head modeling from images for virtual reality systems". The defense has taken place in M.V. Keldysh Research Institute of Applied Mathematics, Russian Academy of Science. Scientific advisors Yuri M. Bayakovsky and Anton S. Konushin. Maxim finished phd program in 2009, but has continued his research until this successful end!
