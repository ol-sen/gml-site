Active: yes
Title: Congrats to winners of UMNIK competition
Category: news
Date: 2018-02-18 00:00

Three students of videogroup of computer graphics and media lab Anzina Aleksandra, Kuptsov Yevgeniy and Kondranin Denis won UMNIK competition. Totally there were five winners in the IT specialization of the competition. [Results](http://www.fasie.ru/press/fund/rezultaty-po-programme-umnik-/) of the competition were announced on 8th February 2018. Send congrats to the winners!
