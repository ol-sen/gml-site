Active: yes
Title: Graduate Qualification Thesis Contest 2017
Category: news
Date: 2017-06-26 00:00

Two of our graduates won [Graduate Qualification Thesis Contest 2017](http://smu.cs.msu.ru/activity/contests/diploma/2017) and their thesis were included into "Collection of abstracts of the best final qualification works of CMC MSU 2017":

- "Detection of vertical camera position mismatch in stereo video", A. Anzina, 2 place
- "Neural network model for human pose estimation", I. Petrov, 3 place

Congratulations to Alexandra and Ilya!
