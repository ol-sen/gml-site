Active: yes
Title: Microsoft Research Computer Vision Contest
Category: news
Date: 2011-11-25 00:00

We are pleased to announce that the registration for Microsoft Research Computer Vision Contest 2012. Registration deadline is January 15, 2012.Students are encouraged to apply their imagination and passion to create novel technological solutions using Kinect for Windows. Demonstrate creativity in your choice of a challenging problem, as well as your computer vision skills! Submission deadline is February 28, 2012.The authors of the top 5 works will travel to Cambridge (United Kingdom) for the Microsoft PhD Summer School 2012.

We kindly ask scientific advisors to pass this information to the students.
