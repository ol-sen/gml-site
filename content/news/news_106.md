Active: yes
Title: The Microsoft Machine Learning and Intelligence School
Category: news
Date: 2015-03-18 00:00

The Microsoft Machine Learning and Intelligence School will take place in Saint Petersburg, Russia, from July 29 to August 5, 2015. The School is sponsored by Microsoft Research and Yandex and is organized in cooperation with Lomonosov Moscow State University (MSU).

The school offers advanced undergraduates, PhD students, and young scientists and developers a unique opportunity to learn about the latest research in machine learning, intelligence and data science from top scientists. The Machine Learning and Intelligence School 2015 will bring together thought leaders and practitioners from a broad range of disciplines including computer science, engineering, statistics, mathematics and economics. Participants will learn some of the key challenges posed by this new era of machine learning and intelligence, and will work on the next generation of approaches, techniques and tools that will be needed to exploit the information revolution for the benefit of society.

The school will have courses, lectures, question and answer sessions, and homework.

Courses: Isabelle Guyon (ChaLearn, USA), Christoph Lampert (Institute of Science and Technology, Austria), Mona Soliman Habib (Microsoft Corporation, USA), Fabrizio Gagliardi (ACM Europe, Europe). Rest of the lecturers to be announced soon.

All participating students will get a one-year complimentary ACM Professional Membership with Digital Library access.

Accepted students pay no tuition, and the school covers their accommodations, meals, and social program. The students pay for their travel to St Petersburg and back.

Qualified candidates should  complete their applications by March 31, 2015. Acceptance decisions will be made around May 15, 2015.

For additional information, visit [http://machinelearning2015.ru/en](http://machinelearning2015.ru/en)
