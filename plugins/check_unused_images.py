from bs4 import BeautifulSoup
import os


image_source_folders = ['./content', './theme/static']
image_target_folders = ['.', './theme']
site_generated_files = 'public'
SITEURL = 'https://faizov_boris.gitlab.io/new-gml-site-arcana'


images_list = []
for image_source_folder, image_target_folder in zip(image_source_folders, image_target_folders):
    for d, _, filenames in os.walk(os.path.join(image_source_folder, 'images')):
        for filename in filenames:
            filename = os.path.join(d, filename)
            filename = os.path.relpath(filename, image_source_folder)
            filename = os.path.join(image_target_folder, filename)
            images_list.append(filename)

used_images_list = []
for d, _, filenames in os.walk(site_generated_files):
    for filename in filenames:
        if filename[-5:] != '.html':
            continue
        filename = os.path.join(d, filename)
        with open(filename, "r") as fp:
            content = fp.read()
        soup = BeautifulSoup(content, features="html.parser")
        for tag_name, tag_property in [('img', 'src'), ('link', 'href')]:
            for img in soup.findAll(tag_name):
                img_instance = img[tag_property]
                if img_instance.startswith(SITEURL):
                    img_instance = img_instance[len(SITEURL):]
                if img_instance[0] == '/':
                    img_instance = img_instance[1:]
                used_images_list.append(os.path.join('.', img_instance))

images_list = set(images_list)
used_images_list = set(used_images_list)
for filename in images_list.difference(used_images_list):
    print("\033[91m", "FOUND UNUSED IMAGE:", filename, "\033[0m")
