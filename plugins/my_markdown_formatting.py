from bs4 import BeautifulSoup
from pelican import signals
import re


def add_line_strikethrough(data_passed_from_pelican):
    if data_passed_from_pelican._content:
        full_content_of_page_or_post = data_passed_from_pelican._content
    else:
        return

    all_instances_of_pre_elements = re.findall(r'(?<!\\)~~.*?(?<!\\)~~', full_content_of_page_or_post, re.DOTALL)
    if(len(all_instances_of_pre_elements) > 0):
        updated_full_content_of_page_or_post = full_content_of_page_or_post
        for pre_element_to_parse in all_instances_of_pre_elements:
            replacement_text = re.sub(r'~~(.*?)~~', r'<s>\1</s>', pre_element_to_parse)
            updated_full_content_of_page_or_post = \
                updated_full_content_of_page_or_post.replace(pre_element_to_parse, replacement_text)
        data_passed_from_pelican._content = updated_full_content_of_page_or_post

    full_content_of_page_or_post = data_passed_from_pelican._content
    all_instances_of_pre_elements = re.findall(r'\\~~.*?\\~~', full_content_of_page_or_post, re.DOTALL)
    if(len(all_instances_of_pre_elements) > 0):
        updated_full_content_of_page_or_post = full_content_of_page_or_post
        for pre_element_to_parse in all_instances_of_pre_elements:
            replacement_text = re.sub(r'\\~~(.*?)\\~~', r'~~\1~~', pre_element_to_parse)
            updated_full_content_of_page_or_post = \
                updated_full_content_of_page_or_post.replace(pre_element_to_parse, replacement_text)
        data_passed_from_pelican._content = updated_full_content_of_page_or_post


def add_image_captions(data_passed_from_pelican):
    if data_passed_from_pelican._content:
        full_content_of_page_or_post = data_passed_from_pelican._content
    else:
        return
    soup = BeautifulSoup(full_content_of_page_or_post, features="html.parser")
    for img in soup.findAll('img'):
        if not img.has_attr('alt'):
            continue
        new_img = '<div class="captionedimage">' \
                  '    <img alt="{alt}" src="{src}" title="{title}">' \
                  '    <p>{alt}</p>' \
                  '</div>'.format(alt=img['alt'], src=img['src'], title=img['title'] if img.has_attr('title') else '')
        img.replaceWith(BeautifulSoup(new_img, features="html.parser"))
    data_passed_from_pelican._content = soup.decode(formatter='html')


def add_table_overflow_x(data_passed_from_pelican):
    if data_passed_from_pelican._content:
        full_content_of_page_or_post = data_passed_from_pelican._content
    else:
        return
    soup = BeautifulSoup(full_content_of_page_or_post, features="html.parser")
    for table in soup.findAll('table'):
        new_table = '<div class="table_container">' +table.decode(formatter='html') + '</div>'
        table.replaceWith(BeautifulSoup(new_table, features="html.parser"))
    data_passed_from_pelican._content = soup.decode(formatter='html')


def register():
    signals.content_object_init.connect(add_line_strikethrough)
    signals.content_object_init.connect(add_image_captions)
    signals.content_object_init.connect(add_table_overflow_x)
